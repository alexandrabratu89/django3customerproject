from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.shortcuts import render, redirect

# Create your views here.
from django.urls import reverse
from django.views import View
from django.views.generic import CreateView
from django.views.generic import ListView
from django.views.generic import UpdateView

from home.forms import LocationForm
from home.models import Location


class HomeIndex(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    permission_required = 'home.can_view_home'
    template_name = 'home/home_index.html'
    context_object_name = "all_locations"
    model = Location

    def get_queryset(self):
        return self.model.objects.all()
    
    # def get_form_kwargs(self):
    #     kwargs = super(HomeIndex, self).get_form_kwargs()
    #     kwargs.update({'a': 1})
    #     return kwargs


class ReadIndex(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    permission_required = 'home.can_view_home'
    template_name = 'home/read_details.html'
    model = Location
    form_class = LocationForm

    def get_queryset(self):
        return self.model.objects.all()

    def get_form_kwargs(self):
        kwargs = super(ReadIndex, self).get_form_kwargs()
        kwargs.update({'pk': self.kwargs['pk']})
        return kwargs


class CreateLocation(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    permission_required = 'home.can_create_home'
    template_name = 'home/location_add.html'
    model = Location
    form_class = LocationForm
    # fields = ['city', 'country']

    def get_form_kwargs(self):
        kwargs = super(CreateLocation, self).get_form_kwargs()
        kwargs.update({'pk': None})
        return kwargs

    def get_success_url(self):
        return reverse('home:update', args=[str(self.object.id)])


class UpdateIndex(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    permission_required = 'home.can_update_home'
    template_name = 'home/location_add.html'
    model = Location
    form_class = LocationForm

    def get_queryset(self):
        return self.model.objects.all()

    def get_form_kwargs(self):
        kwargs = super(UpdateIndex, self).get_form_kwargs()
        kwargs.update({'pk': self.kwargs['pk']})
        return kwargs

    def get_success_url(self):
        return reverse('home:update', args=[str(self.object.id)])


