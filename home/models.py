from django.db import models

# Create your models here.


class Location(models.Model):

    choicesCity = (('Bucharest', 'Bucharest'),
                   ('Craiova', 'Craiova'),
                   ('Iasi', 'Iasi'))

    choicesCountry = (('Romania', 'Romania'),)

    city = models.CharField('City', max_length=100)
    country = models.CharField('Country', max_length=100)

    def __str__(self):
        return '{} {}'.format(str(self.city), str(self.country))

class Customer(models.Model):
    name = models.CharField('Name', max_length=50)
    location = models.ForeignKey(Location)
