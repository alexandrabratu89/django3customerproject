from django import forms
from django.forms import Select, TextInput

from home.models import Location


class LocationForm(forms.ModelForm):
    class Meta:
        model = Location
        fields = ['city', 'country']

        widgets = {
            'city': TextInput(attrs={'placeholder': 'City', 'class': 'form-control'}),
            'country': TextInput(attrs={'placeholder': 'Country', 'class': 'form-control'}),
        }

    def __init__(self, pk, *args, **kwargs):
        super(LocationForm, self).__init__(*args, **kwargs)
        self.pk = pk
        pass

    def clean(self):
        cleaned_data = self.cleaned_data
        print(self.pk, 'pk>>>')
        city_value = self.cleaned_data['city']
        country_value = self.cleaned_data['country']
        if self.pk:
            all_locations = Location.objects.all().exclude(id=self.pk)
        else:
            all_locations = Location.objects.all()
        for location in all_locations:
            if location.city == city_value and location.country == country_value:
                self._errors['city'] = 'Duplicate entry'
        return cleaned_data

